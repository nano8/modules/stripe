import { defineConfig } from 'vitepress';

export default defineConfig({
    lastUpdated:   true,
    sitemap:       {
        hostname: 'https://nano8.gitlab.io/modules/stripe/',
    },
    title:         'nano/modules/stripe',
    titleTemplate: 'nano/modules/stripe - documentation',
    description:   'stripe module for laylatichy/nano framework',
    base:          '/modules/stripe/',
    themeConfig:   {
        lastUpdated: {
            text:          'updated',
            formatOptions: {
                dateStyle: 'long',
                timeStyle: 'medium',
            },
        },
        search:      {
            provider: 'local',
        },
        nav:         [],
        sidebar:     [
            {
                text:  'getting started',
                items: [
                    {
                        text: 'introduction',
                        link: '/getting-started/introduction',
                    },
                    {
                        text: 'installation',
                        link: '/getting-started/installation',
                    },
                ],
            },
            {
                text:  'usage',
                items: [
                    {
                        text: 'useStripe',
                        link: '/usage/use-stripe',
                    },
                ],
            },
            {
                text: 'nano documentation',
                link: 'https://nano.laylatichy.com',
            },
        ],
        socialLinks: [
            {
                icon: 'github',
                link: 'https://gitlab.com/nano8/modules/stripe',
            },
        ],
        editLink:    {
            text:    'edit this page',
            pattern: 'https://gitlab.com/nano8/modules/stripe/-/edit/dev/docs/:path',
        },
    },
});
