---
layout: doc
---

## installation

you can install this module using composer

```sh
composer require laylatichy/nano-modules-stripe
```

## registering the module

```php
use laylatichy\nano\modules\stripe\StripeModule;

useNano()->withModule(new StripeModule('api_key'));
```

