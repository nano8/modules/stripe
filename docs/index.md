---
layout: home

hero:
    name:    nano/modules/stripe
    tagline: stripe module for nano
    actions:
        -   theme: brand
            text:  get started
            link:  /getting-started/introduction

features:
    -   title:   simple and minimal, always
        details: |
                 nano is a simple and minimal framework, and so are its modules. nano/modules/stripe is a simple and minimal module for stripe integration
    -   title:   stripe integration
        details: |
                 nano/modules/stripe provides a simple way to use stripe
---
