---
layout: doc
---

<script setup>
const args = {
    useStripe: [],
};
</script>

## usage

`useStripe` is a hook that returns the stripe client

## <Types fn="useStripe" r="StripeClient" :args="args.useStripe" /> {#useStripe}

```php
useStripe();
```