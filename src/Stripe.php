<?php

namespace laylatichy\nano\modules\stripe;

use Stripe\StripeClient;

class Stripe {
    public StripeClient $client;

    public function __construct(string $key) {
        $this->client = new StripeClient($key);
    }
}
