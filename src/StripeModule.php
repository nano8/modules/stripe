<?php

namespace laylatichy\nano\modules\stripe;

use laylatichy\nano\modules\NanoModule;
use laylatichy\nano\Nano;

class StripeModule implements NanoModule {
    public Stripe $stripe;

    public function __construct(private readonly string $key) {
        // nothing to do here
    }

    public function register(Nano $nano): void {
        if (isset($this->stripe)) {
            useNanoException('stripe module already registered');
        }

        $this->stripe = new Stripe($this->key);
    }
}
