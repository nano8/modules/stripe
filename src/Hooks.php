<?php

use laylatichy\nano\modules\stripe\StripeModule;
use Stripe\StripeClient;

if (!function_exists('useStripe')) {
    function useStripe(): StripeClient {
        return useNanoModule(StripeModule::class)
            ->stripe
            ->client;
    }
}